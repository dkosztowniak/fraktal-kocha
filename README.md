# Fraktal Kocha
Grafika żółwia w Pythonie. Przykład z rysowaniem krzywej Kocha i płatka Kocha.

![alt text](images/krzywaKochaPy.png "Krzywa Kocha")

Na początku programu konieczne jest zaimportowanie modułu turtle odpowiedzialnego za grafikę żółwia w Pythonie.
```python
import turtle
```
Więcej informacji na stronie: [Krzywa Kocha »](https://ciekawawiedza.blogspot.com/2021/04/krzywa-kocha.html)
